import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AcceptComponent, DialogComponent, AppComponent,PopupComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MatButtonModule, MatCardModule, MatMenuModule, MatToolbarModule, MatIconModule, MatSidenavModule, MatCheckboxModule, MatGridListModule, MatSliderModule, MatDialogModule, MatDialogRef, MatSlideToggleModule, MatDividerModule, MatListModule, MatTableModule, MatProgressBarModule} from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ChartsAnnotation} from 'chartjs-plugin-annotation'
import { ChartsModule } from 'ng2-charts';
import { SwiperModule, SwiperConfigInterface } from 'ngx-swiper-wrapper';
import 'nouislider';
import 'hammerjs';

const SWIPER_CONFIG: SwiperConfigInterface = {
    observer: true,
    direction: 'horizontal',
    threshold: 50,
    spaceBetween: 5,
    slidesPerView: 3,
    centeredSlides: true,
    initialSlide:1,
    autoplay: 3000
  };

@NgModule({
   declarations: [
		AcceptComponent,
        DialogComponent,
        AppComponent,
        PopupComponent
   ],
   entryComponents: [
		AcceptComponent,
        DialogComponent,
        PopupComponent
   ],
   imports: [
      BrowserModule,
      HttpModule,
      MatButtonModule,
      MatMenuModule,
      MatCardModule,
      MatToolbarModule,
      MatIconModule,
      BrowserAnimationsModule,
      MatSidenavModule,
      MatCheckboxModule,
      MatGridListModule,
      MatSliderModule,
      MatDialogModule,
      MatSlideToggleModule,
      MatDividerModule,
      MatListModule,
      MatTableModule,
      MatProgressBarModule,
      ChartsModule,
      FormsModule,
      SwiperModule.forRoot(SWIPER_CONFIG)
   ],
   providers: [],
   bootstrap: [AppComponent]
})
export class AppModule { }

