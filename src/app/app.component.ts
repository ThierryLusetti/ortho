import {
  Component,
  OnInit,
  Inject,
  ViewChild
} from '@angular/core';

import {
  MatDialog,
  MatDialogConfig,
  MatDialogRef,
  MAT_DIALOG_DATA,
  MatProgressBar,
} from '@angular/material';

import {
  BaseChartDirective
} from 'ng2-charts/ng2-charts';

import {
  Http
} from '@angular/http'

import 'chartjs-plugin-annotation';
import * as noUiSlider from 'nouislider';

// Accept Component
@Component({
  selector: 'AcceptSelector',
  templateUrl: './accept.component.html',
  styleUrls: ['./accept.component.css']
})
export class AcceptComponent {
  constructor(@Inject(MAT_DIALOG_DATA) public data: any, public dialogRef: MatDialogRef < any > ) {}

  closeDialog(): void {
    this.dialogRef.close();
  }
}


// Dialog Component
@Component({
  selector: 'TreatmentSelection',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css']
})
export class DialogComponent {
  constructor(public dialogRef: MatDialogRef < any > ) {}
  selected = 2
  onNoClick(): void {
    this.dialogRef.close(this.selected);
  }
  select1(): void {
    this.selected = 1;
    this.dialogRef.close(this.selected);
  }
  select2(): void {
    this.selected = 2;
    this.dialogRef.close(this.selected);
  }
  select3(): void {
    this.selected = 3;
    this.dialogRef.close(this.selected);
  }
  
}

@Component({
  selector: 'Popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.css']
})
export class PopupComponent {
  constructor(@Inject(MAT_DIALOG_DATA) public data: any, public dialogRef: MatDialogRef < any > ) {}
  closePopup():void {
    this.dialogRef.close();
  }
}

// App Component
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  @ViewChild(BaseChartDirective)
  chart: BaseChartDirective;
  // Dialog constructor
  constructor(public dialog: MatDialog,
    private _httpService: Http) {}

  apiValues: string[] = [];

  ngOnInit() {
    this._httpService.get('/api/values').subscribe(values => {
      this.apiValues = values.json() as string[];
    });
    this.graphData();
    this.createslider();
  }


  createslider(): void {
    var downpayment = document.getElementById('downpayment') as noUiSlider.Instance;
    var monthlypayment = document.getElementById('monthlypayment') as noUiSlider.Instance;
    var sliderlabel = document.getElementsByClassName('noUi-tooltip') as HTMLCollectionOf<HTMLDivElement>;
    var finance = document.getElementById('finance-extra') as HTMLDivElement;
    console.log(finance);
    var that = this;
	
    noUiSlider.create(downpayment, {
      direction: 'rtl', // Put '0' at the bottom of the slider
      orientation: 'vertical', // Orient the slider vertically
      behaviour: 'tap-drag', // Move handle on tap, bar is draggable
      start: this.downPaymentVar,
      connect: [true, false],
      step: 10,
      tooltips: true,
      range: {
        'min': 580,
        'max': this.grandTotalVar
      }
    });
    downpayment.noUiSlider.on('update', function () {
      that.downPaymentVar = Number(downpayment.noUiSlider.get());
      sliderlabel[0].innerHTML = "$" + that.downPaymentVar.toLocaleString();
      that.graphData();
      if (that.financeExists()){
        let position = (that.monthlyPaymentVar-100)/400*350;
        finance.style.display = 'block';
        finance.style.bottom = position+'px';
      } else {
        finance.style.display = 'none';
      }
    });

    noUiSlider.create(monthlypayment, {
      direction: 'rtl', // Put '0' at the bottom of the slider
      orientation: 'vertical', // Orient the slider vertically
      behaviour: 'tap-drag', // Move handle on tap, bar is draggable
      start: this.monthlyPaymentVar,
      connect: [true, false],
      tooltips: true,
      step: 10,
      range: {
        'min': 100,
        'max': 500
      }
    });
    monthlypayment.noUiSlider.on('update', function () {
      that.monthlyPaymentVar = Number(monthlypayment.noUiSlider.get());
      sliderlabel[1].innerHTML = "$" + that.monthlyPaymentVar.toLocaleString();
      that.graphData();
      if (that.financeExists()){
        let position = (that.monthlyPaymentVar-100)/400*350;
        finance.style.display = 'block';
        finance.style.bottom = position+'px';
      } else {
        finance.style.display = 'none';
      }
    });
  }
  treatmentSelect(): void {
    let dialogRef = this.dialog.open(DialogComponent, {
      panelClass: 'treatmentSelect-panel',
      height: '100%',
      width: '100%'
    });
    let app = document.getElementById('app') as HTMLDivElement;
    app.style.filter= 'blur(2.5px)';
    dialogRef.afterClosed().subscribe(result => {
      this.setVars(result);
      app.style.filter= 'none';
    });
  }

  fullPayment(): void {
    this.downPaymentVar = this.grandTotal();
    var slider = document.getElementById('downpayment') as noUiSlider.Instance;
    slider.noUiSlider.set(this.downPaymentVar);
  }
  popularPayment(): void {
    this.downPaymentVar = Math.round(this.grandTotal() * 0.33)
    var slider = document.getElementById('downpayment') as noUiSlider.Instance;
    slider.noUiSlider.set(this.downPaymentVar);
  }
  lowestPayment(): void {
    var slider = document.getElementById('downpayment') as noUiSlider.Instance;
    this.downPaymentVar = 500
    slider.noUiSlider.set(this.downPaymentVar);
  }

  accept(): void {
    let addonData = [];
    if(this.extraoption1){
      let extra1 = {label:this.add_on_title_1, value:this.add_on_total_1};
      addonData.push(extra1);
    }
    if(this.extraoption2){
      let extra2 = {label:this.add_on_title_2, value:this.add_on_total_2};
      addonData.push(extra2);
    }
    let dialogRef = this.dialog.open(AcceptComponent, {
      panelClass: 'my-full-screen-dialog',
      height: '100%',
      width: '100%',
      data: {
        patient: this.first_name + ' ' + this.last_name,
        treatment_name: this.treatmentTitleVar,
        treatment_length: this.treatmentTermVar,
        payment_term: this.paymentTermVar,
        initial_payment: this.downPaymentVar,
        monthly_payment: this.monthlyPaymentVar,
        treatment_total: this.treatmentTotalVar,
        grandTotalVar: this.grandTotalVar,
        financeExist:this.financeExists(),
        financeAmount:this.financeAmount(),
        addonTreatmentTotal: this.treatmentTotal() + this.extraoption1 * this.add_on_total_1 + this.extraoption2 * this.add_on_total_2,
        addonAmount: addonData
      }
    });
  }

  openPopup(info: number):void{
    console.log(this.dialogdata[info]);
    let dialogRef = this.dialog.open(PopupComponent, {
      width: '40%',
      data: {
        title: this.dialogdata[info].title,
        content: this.dialogdata[info].content,
        cost:this.dialogdata[info].cost
      }
    });
  }

  setVars(selected: number): void {
    console.log('Selected: ' + selected)
    this.chosen_treatment = selected;

    if (this.chosen_treatment == 1) {
      this.treatmentTitleVar = this.treatment_title_1;
      this.treatmentDescVar = this.treatment_desc_1;
      this.treatmentTotalVar = this.treatment_total_1;
      this.treatmentTermVar = this.treatment_term_1;
      console.log('In 1')
    } else if (this.chosen_treatment == 2) {
      this.treatmentTitleVar = this.treatment_title_2;
      this.treatmentDescVar = this.treatment_desc_2;
      this.treatmentTotalVar = this.treatment_total_2;
      this.treatmentTermVar = this.treatment_term_2;
      console.log('In 2')
    } else if (this.chosen_treatment == 3) {
      this.treatmentTitleVar = this.treatment_title_3;
      this.treatmentDescVar = this.treatment_desc_3;
      this.treatmentTotalVar = this.treatment_total_3;
      this.treatmentTermVar = this.treatment_term_3;
      console.log('In 3')
    }
  }

  chosen_treatment = 2;

  first_name = 'Jamie';
  last_name = 'Townsend';

  treatment_total_1 = 5600;
  treatment_total_2 = 5800;
  treatment_total_3 = 5300;

  treatment_term_1 = 24;
  treatment_term_2 = 24;
  treatment_term_3 = 24;

  treatment_title_1 = 'Damon Clear Braces'
  treatment_title_2 = 'Invisalign'
  treatment_title_3 = 'Damon Metal Braces'

  treatment_desc_1 = 'Virtually invisible, Damon Clear has clear advantages over traditional braces and aligners.  Damon Clear is part of the Innovative Damon System, which combines tireless braces with high technology...'
  treatment_desc_2 = 'Choose the clear aligner system committed to continuous innovation, with 20 years of clinical resaerch and more than 700 patents.  No other clear aligner is backed by the data and exprerience of 5 million cases.'
  treatment_desc_3 = "The Damon System is not just about revolutionary braces and wires, it's a whole new way of treating patients.  Traditional treatment oftern requires removal of healthy teeth and/or use of palatal expanders."

  add_on_title_1 = 'Wildsmiles'
  add_on_title_2 = 'Extra retainer'
  add_on_title_3 = 'Clear Brackets'

  add_on_desc_1 = 'Personalize with fun shapes...wild!!!'
  add_on_desc_2 = "Because you know you're going to forget your main one on your next road trip."
  add_on_desc_3 = 'Great for a clean, understated look'

  add_on_total_1 = 250;
  add_on_total_2 = 500;

  //
  blueMessage = true;
  greenMessage = false;
  yellowMessage = true;
  //

  treatmentTitleVar = this.treatment_title_2;
  treatmentDescVar = this.treatment_desc_2;
  treatmentTotalVar = this.treatment_total_2;
  treatmentTermVar = this.treatment_term_2;

  preDiscountVar = 4800;
  grandTotalVar = 0;
  downPaymentVar = 1000;

  extraoption1 = 0;
  extraoption2 = 0;
  insuranceVar = 1000;

  paymentTermVar = 76;
  totalValueVar = 0;
  discountPercentVar = 0;
  discountAmountVar = 0;
  monthlyPaymentVar = 100;

  monthProgress = 1;
  dialogdata = [
    {
      title:'Extra Retainers',
      content:'This type of retainer is used for long-term treatment, as they are semi-permanent. They stay in place at all times and should only be removed by Adirondack Orthdontics or another experienced provides.',
      cost:'Extra Cost: $500',
    }
  ];
  chartData = [];
  dataArray = [];
  /*
    chartData = [{
		borderColor: '#AAAAC0',
		borderWidth: 5,
        data: [{
            x: 0,
            y: this.treatmentTotalVar
        }],
        label: ''
    }];
	*/

  public chartColors: any[] = [{
    backgroundColor: ["#E6E6E6"]
  }];

  chartOptions = {
    bezierCurve: true,
    responsive: true,
    maintainAspectRatio: false,
    layout: {
      padding: {
        left: -10,
        bottom: -10
      }
    },
    /*
        elements: {
            point: {
                radius: 0
            }
        },
		*/
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true,
          min: 0,
          max: 5100,
          display: false,
          mirror: true
        },
        gridLines: {
          display: false,
          drawBorder: false
        }
      }],
      xAxes: [{
        type: 'linear',
        ticks: {
          display: false,
          stepSize: 12,
          min: 0,
          max: 48,
          beginAtZero: true,
          mirror: true
        },
        gridLines: {
          display: false
        }
      }]
    },
    annotation: {
      annotations: [{
        type: 'line',
        id: 'treatmentLine',
        mode: 'vertical',
        scaleID: 'x-axis-0',
        value: this.treatmentTermVar - 1,
        borderWidth: 2,
        borderColor: 'black',
        borderDash: [2, 2],
        borderDashOffset: 5,

        label: {
          backgroundColor: 'black',
          enabled: true,
          content: "Treatment Complete",
          position: "top"
        }
      }]
    }
  }


  treatmentTitle(): string {
    return this.treatmentTitleVar;
  }

  treatmentDesc(): string {
    return this.treatmentDescVar;
  }

  treatmentTotal(): number {
    return this.treatmentTotalVar;
  }

  preDiscount(): number {
	  this.preDiscountVar = this.totalValue() - this.insuranceVar;
	  return this.preDiscountVar;
  }
  
  discountExists(): boolean {
	var preDiscount = this.totalValue() - this.insuranceVar;
    if (this.downPaymentVar >= Math.ceil(0.75 * preDiscount)) {
      return true;
    } else {
      return false;
    }
  }
  checkDiscount(): number {
	var preDiscount = this.totalValue() - this.insuranceVar; 
	if (this.downPaymentVar >= preDiscount) {
      this.discountPercentVar = 0.04;
    }
	else if (this.downPaymentVar >= Math.ceil(0.75 * preDiscount)) {
      this.discountPercentVar = 0.02;
    }	
	else {
      this.discountPercentVar = 0;
    }
    return this.discountPercentVar;
  }

  financeExists(): boolean {
    if (this.paymentTerm() > this.treatmentTermVar) {
      return true;
    } else {
      return false;
    }
  }
  
  financeAmount(): number {
    return Math.ceil((this.totalValue() - this.downPaymentVar - this.insuranceVar) * 0.06);
  }
  
  financeAmountMonth(): number{
    return Math.round((100 * this.financeAmount() / this.paymentTerm()) / 100);
  }

  financeMessage(): string {
    var decimal = Math.round((100 * this.financeAmount() / this.paymentTerm()) / 100);
    return '*$' + this.monthlyPaymentVar + ' + $' + decimal + '/mo finance charges'
  }

  monthlyPayment(): number {
    if (this.financeExists()) {
      var monthly = this.monthlyPaymentVar + this.financeAmount() / this.paymentTermVar;
      var decimal = Math.round((100 * monthly) / 100);
      return decimal;
    } else {
      return this.monthlyPaymentVar
    }
  }

  totalValue(): number {
    this.totalValueVar = this.treatmentTotal() + this.extraoption1 * this.add_on_total_1 + this.extraoption2 * this.add_on_total_2;
    return this.totalValueVar;
  }

  discountAmount(): number {
    this.checkDiscount();
    this.discountAmountVar = this.totalValue() * this.checkDiscount();
    return this.discountAmountVar;
  }

  grandTotal(): number {
    this.grandTotalVar = this.totalValue() - this.discountAmount() - this.insuranceVar;
    return this.grandTotalVar;
  }

  paymentTerm(): number {
    if ((this.grandTotal() - this.downPaymentVar) <= 0) {
      this.paymentTermVar = 0;
      return this.paymentTermVar;
    }
    this.paymentTermVar = Math.ceil(Number((this.grandTotal() - this.downPaymentVar) / this.monthlyPaymentVar));
    return this.paymentTermVar;
  }

  // Sloppy add of dynamic messages
  messageBox(): string {
    if (this.discountAmount() > 0) {
      this.blueMessage = false;
      this.greenMessage = true;
      this.yellowMessage = false;
      return 'You got a $' + this.discountAmount() + ' Discount!'
    } else if (this.downPaymentVar <= 500) {
      this.blueMessage = false;
      this.greenMessage = false;
      this.yellowMessage = true;
      return '$500 is the minimum allowed down payment.'
    } else if (this.paymentTerm() > this.treatmentTermVar) {
      this.blueMessage = false;
      this.greenMessage = false;
      this.yellowMessage = true;
      return 'Payment terms that go beyond the Treatment length will incur finance charges.'
    } else {
      this.blueMessage = true;
      this.greenMessage = false;
      this.yellowMessage = false;
      return 'Make a down payment of at least $' + Math.ceil(0.75 * this.grandTotal()) + ' and get a 2% Discount or pay in full and get a 4% Discount.'
    }
  }


  graphData(): void {
    this.chartData.length = 0;
    this.dataArray.length = 0;
    //var remaining = this.grandTotal() - this.downPaymentVar;
	var remaining = 5000;
	
	//
	this.dataArray.push({
      x: 0,
      y: remaining
    });
	//
	
    //
    const bez1 = [(this.paymentTerm() / 10) - 1, remaining * 1]; //0.9
    const bez2 = [(this.paymentTerm() / 10 * 2) - 1, remaining * 0.96]; //0.8
    const bez3 = [(this.paymentTerm() / 10 * 3) - 1, remaining * 0.88]; //0.7
    const bez4 = [(this.paymentTerm() / 10 * 4) - 1, remaining * 0.74]; //0.6
    const bez5 = [(this.paymentTerm() / 10 * 5) - 1, remaining * 0.59]; //0.5
    const bez6 = [(this.paymentTerm() / 10 * 6) - 1, remaining * 0.40]; //0.4
    const bez7 = [(this.paymentTerm() / 10 * 7) - 1, remaining * 0.22]; //0.3
    const bez8 = [(this.paymentTerm() / 10 * 8) - 1, remaining * 0.08]; //0.2
    const bez9 = [(this.paymentTerm() / 10 * 9) - 1, remaining * 0.02]; //0.1
    //
    
    // Only add if there's enough room it doesn't look stupid
    this.dataArray.push({x: bez1[0],y: bez1[1]});
    this.dataArray.push({x: bez2[0], y: bez2[1]});
    this.dataArray.push({x: bez3[0], y: bez3[1]});
    this.dataArray.push({x: bez4[0], y: bez4[1]});
    this.dataArray.push({x: bez5[0], y: bez5[1]});
    this.dataArray.push({x: bez6[0], y: bez6[1]});
    this.dataArray.push({x: bez7[0], y: bez7[1]});
    this.dataArray.push({x: bez8[0],y: bez8[1]});
    this.dataArray.push({x: bez9[0], y: bez9[1]});
    //

    this.dataArray.push({x: this.paymentTermVar - 1, y: 0});


    // Load images
    var birthdayImage = new Image();
    birthdayImage.src = '/assets/Birthday.svg'
    var dateImage = new Image();
    //dateImage.src = '/assets/Date.svg'
	dateImage.src = 'assets/months/' + this.paymentTermVar + '.svg'


    // Add chart data points
    this.chartData = [
      // Treatment Complete Data Set
      /*
      	{
      		borderColor: 'transparent',
      		borderWidth: 0,
      		pointRadius: 5,
      		pointStyle: dateImage,
      		fill: false,
      		data: [{x: this.paymentTermVar - 1, y: 0}],
      		label: ''
      	},
      */
	  // Year Data Set
      // Birthday Data Set
      {
        borderColor: 'transparent',
        borderWidth: 0,
        pointRadius: 5,
        pointStyle: birthdayImage,
        fill: false,
        data: [{x: 4, y: 0}, {x: 16, y: 0},{x: 28, y: 0}, {x:40, y:0}],
        label: ''
      },
      // Actual Data Set
      {
        /*
        borderColor: '#AAAAC0',
        */
        borderColor: '#F2C42F',
        borderWidth: 10,
        pointRadius: 0,
        pointStyle: ['', '', '', '', '', '', '', '', '', '', dateImage],
        fill: true,
        data: this.dataArray,
        label: ''
      }
    ];

    if (this.chart.chart) {
	// Update the date image
	  this.chart.chart.data.datasets[1].pointStyle[10] = dateImage;
      if (this.paymentTermVar > this.treatmentTermVar) {
        this.chart.chart.data.datasets[1].borderColor = '#F2C42F';
        this.chart.chart.data.datasets[1].borderWidth = 10;
      } else {
        this.chart.chart.data.datasets[1].borderColor = '#AAAAC0';
        this.chart.chart.data.datasets[1].borderWidth = 5;
      }
    }

    this.monthProgress = this.paymentTermVar / 48 * 100 - 2;
  }

}
